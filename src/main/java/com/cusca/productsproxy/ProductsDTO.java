package com.cusca.productsproxy;

import lombok.Data;

@Data
public class ProductsDTO {
   private int id;
   private String title;
   private String price;
   private String  category;
   private String description;
   private String image;

}
